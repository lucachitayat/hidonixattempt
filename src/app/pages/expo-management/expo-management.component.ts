import { NgFor } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { SidebarComponent } from '../../components/sidebar/sidebar.component';
import { NavbarComponent } from '../../components/navbar/navbar.component';

interface Pavilion {
  name: string;
  description: string;
  imageUrl: string;
  poweredBy?: string;
  collaboration?: string;
}
@Component({
  selector: 'app-expo-management',
  standalone: true,
  imports: [NgFor, SidebarComponent, NavbarComponent],
  templateUrl: './expo-management.component.html',
  styleUrl: './expo-management.component.scss',
})
export class ExpoManagementComponent implements OnInit {
  pavilions: Pavilion[] = [];

  constructor() {}

  ngOnInit(): void {
    this.pavilions = [
      {
        name: 'Pavilion 1',
        description: 'GAMING ZONE',
        imageUrl: 'assets/images/pavilion-1.png',
        poweredBy: 'GAMESTOP',
        collaboration: 'Hidonix',
      },
      {
        name: 'Pavilion 2',
        description: 'INTEL ESPORTS SHOW',
        imageUrl: 'assets/images/pavilion-2.png',
        poweredBy: '--',
        collaboration: 'Hidonix',
      },
      {
        name: 'Pavilion 3',
        description: 'INDIE DUNGEON',
        imageUrl: 'assets/images/pavilion-3.png',
        poweredBy: 'GAMESTOP',
        collaboration: 'Hidonix',
      },
      {
        name: 'Pavilion 4',
        description: 'ARCADE UNIVERSE',
        imageUrl: 'assets/images/pavilion-4.png',
        poweredBy: 'Artigan Cab',
        collaboration: 'Hidonix',
      },
      {
        name: 'Pavilion 5',
        description: 'This is the first pavilion',
        imageUrl: 'assets/images/pavilion-5.png',
        poweredBy: 'Angular',
        collaboration: 'Hidonix',
      },
      {
        name: 'Pavilion 6',
        description: 'This is the second pavilion',
        imageUrl: 'assets/images/pavilion-6.png',
        poweredBy: 'Angular',
        collaboration: 'Hidonix',
      },
      {
        name: 'Pavilion 7',
        description: 'This is the third pavilion',
        imageUrl: 'assets/images/pavilion-7.png',
        poweredBy: 'Angular',
        collaboration: 'Hidonix',
      },
      {
        name: 'Pavilion 8',
        description: 'This is the fourth pavilion',
        imageUrl: 'assets/images/pavilion-8.png',
        poweredBy: 'Angular',
        collaboration: 'Hidonix',
      },
    ];
  }
}
