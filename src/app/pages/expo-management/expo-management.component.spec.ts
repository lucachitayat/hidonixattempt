import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpoManagementComponent } from './expo-management.component';

describe('ExpoManagementComponent', () => {
  let component: ExpoManagementComponent;
  let fixture: ComponentFixture<ExpoManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ExpoManagementComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpoManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
