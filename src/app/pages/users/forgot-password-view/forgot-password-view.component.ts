import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-forgot-password-view',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './forgot-password-view.component.html',
  styleUrl: './forgot-password-view.component.scss',
})
export class ForgotPasswordViewComponent {
  emailForm!: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.emailForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  onSubmit = () => {
    this.authService.onForgotPassword(this.emailForm.value.email);
  };
}
