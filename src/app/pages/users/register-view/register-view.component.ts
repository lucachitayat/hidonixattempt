import { Component, inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-register-view',
  standalone: true,
  imports: [ReactiveFormsModule, RouterLinkActive, RouterLink, NgFor],
  templateUrl: './register-view.component.html',
  styleUrl: './register-view.component.scss',
})
export class RegisterViewComponent {
  registerForm!: FormGroup;

  roles = ['Supervisor', 'Administrator'];

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.registerForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
      role: new FormControl(this.roles[0], Validators.required),
    });
  }

  onSubmit = () => {
    this.authService.onSignup(this.registerForm);
  };
}
