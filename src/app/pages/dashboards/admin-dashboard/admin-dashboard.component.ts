import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { NgFor } from '@angular/common';
import { IUser } from '../../../models/auth.model';

@Component({
  selector: 'app-admin-dashboard',
  standalone: true,
  imports: [NgFor],
  templateUrl: './admin-dashboard.component.html',
  styleUrl: './admin-dashboard.component.scss',
})
export class AdminDashboardComponent {
  constructor(private authService: AuthService) {
    this.authService.getCurrentUser().subscribe((value) => {
      this.currentUser = value;
    });
  }

  currentUser: IUser | null = null;

  userList = this.authService.getUserList();

  deleteUser = (id: number) => {
    this.authService.onDeleteUser(id);
    this.userList = this.authService.userList;
  };
}
