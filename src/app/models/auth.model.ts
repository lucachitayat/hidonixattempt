export interface IUser {
  id: number;
  email: string;
  username: string;
  role: string;
}

export interface ILogin {
  email: string;
  password: string;
}

export interface IRegister {
  id: number;
  email: string;
  username: string;
  password: string;
  role: string;
}
