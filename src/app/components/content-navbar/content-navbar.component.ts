import { NgFor } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';

@Component({
  selector: 'app-content-navbar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, NgFor],
  templateUrl: './content-navbar.component.html',
  styleUrl: './content-navbar.component.scss',
})
export class ContentNavbarComponent {
  navItems = [
    'Pavilions',
    'Sectors',
    'Stand',
    'POI',
    'Merchandise Categories',
    'Brands Represented',
    'Brand',
    'Business Categories',
    'Events',
    'Store',
  ];
}
