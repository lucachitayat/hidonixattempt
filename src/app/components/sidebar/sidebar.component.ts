import { Component } from '@angular/core';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, NgFor],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss',
})
export class SidebarComponent {
  constructor(private router: Router, private authService: AuthService) {}
  brandLogo = '/assets/images/logo.png';

  isLoggedIn$: boolean = false;
  userRole$: string = '';

  sidebarItems = [
    { title: 'Exhibitions' },
    {
      title: 'Maps and Tours',
      children: ['Exhibition', 'Outdoor'],
    },
    {
      title: 'Control Room',
      children: ['Exhibitions', 'Outdoor'],
    },
    { title: 'Store' },
    { title: 'Marketing' },
    { title: 'Analytics' },
    { title: 'Help' },
    { title: 'Settings' },
  ];

  ngOnInit(): void {
    this.authService.getIsLoggedIn().subscribe((value) => {
      this.isLoggedIn$ = value;
    });

    this.authService.getCurrentUser().subscribe((value) => {
      this.userRole$ = value.role;
    });
  }
}
