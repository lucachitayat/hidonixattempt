import { Component, OnInit } from '@angular/core';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
})
export class NavbarComponent implements OnInit {
  isLoggedIn$: boolean = false;

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.getIsLoggedIn().subscribe((value) => {
      // console.log('navbar status: ', value);
      this.isLoggedIn$ = value;
    });
  }

  loginPressed(): void {
    this.router.navigate(['login']);
  }

  registerPressed(): void {
    this.router.navigate(['register']);
  }

  logoutPressed(): void {
    this.authService.signOut();
  }
}
