import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AuthService } from './services/auth.service';
import { ContentNavbarComponent } from './components/content-navbar/content-navbar.component';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterOutlet,
    NgbModule,
    NavbarComponent,
    ContentNavbarComponent,
    SidebarComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  constructor(
    private modalService: NgbModal,
    private authService: AuthService
  ) {}

  isLoggedIn$: boolean = false;

  ngOnInit(): void {
    this.authService.getIsLoggedIn().subscribe((value) => {
      // console.log('app status: ', value);
      this.isLoggedIn$ = value;
    });
  }

  public open(modal: any): void {
    this.modalService.open(modal);
  }

  title: string = 'Hidonix';
}
