import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IRegister, IUser } from '../models/auth.model';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isLoggedIn = new BehaviorSubject<boolean>(false);
  private currentUserSubject: BehaviorSubject<IUser> =
    new BehaviorSubject<IUser>({ id: 0, email: '', role: '', username: '' });
  userList: IRegister[] = [];

  private getDataFromLocalStorage(key: string): any {
    const data = localStorage.getItem(key);
    return data ? JSON.parse(data) : [];
  }

  constructor(private router: Router) {
    this.userList = this.getDataFromLocalStorage('signupList');

    let currentUserData = localStorage.getItem('currentUser');

    if (currentUserData) {
      const parsedUser = JSON.parse(currentUserData);
      this.currentUserSubject.next(parsedUser);
      this.changeLoginStatus(parsedUser.id !== 0);
    }
  }

  changeLoginStatus(status: boolean): void {
    this.isLoggedIn.next(status);
  }

  onSignup(registerForm: FormGroup<any>) {
    this.getDataFromLocalStorage('signupList');

    let userId: number;

    if (this.userList.length === 0) {
      userId = 1;
    } else {
      userId = this.userList[this.userList.length - 1].id + 1;
    }

    const userSubmit: IRegister = {
      id: userId,
      email: registerForm.value.email,
      username: registerForm.value.username,
      password: registerForm.value.password,
      role: registerForm.value.role,
    };

    this.userList.push(userSubmit);
    localStorage.setItem('signupList', JSON.stringify(this.userList));
    alert('Account Registered Successfully');
    this.router.navigate(['/login']);
  }

  onLogin(loginForm: FormGroup<any>): void {
    this.getDataFromLocalStorage('signupList');

    const lookupList: IRegister[] = this.userList;

    const validMatch = lookupList.find(
      (registeredJSON) =>
        registeredJSON.email.toLowerCase() ===
          loginForm.value.email.toLowerCase() &&
        registeredJSON.password === loginForm.value.password
    );

    if (!!validMatch) {
      localStorage.setItem('currentUser', JSON.stringify(validMatch));
      this.currentUserSubject.next(validMatch);
      this.changeLoginStatus(true);
      alert('Login successful');
      this.router.navigate(['/pavilions']);
    } else {
      alert('Login failed, please try again.');
    }
  }

  signOut(): void {
    localStorage.removeItem('currentUser');
    this.changeLoginStatus(false);
    alert('Logged out successfully');
    this.router.navigate(['/login']);
  }

  onDeleteUser(id: number): void {
    this.userList = this.userList.filter((user) => user.id !== id);
    localStorage.setItem('signupList', JSON.stringify(this.userList));
    const updateData = localStorage.getItem('signupList');
    if (updateData) {
      this.userList = JSON.parse(updateData);
    }
    alert('User deleted successfully');
    this.router.navigate(['/admin']);
  }

  onForgotPassword(email: string): void {
    this.getDataFromLocalStorage('signupList');
    const user = this.userList.find(
      (user) => user.email.toLowerCase() === email.toLowerCase()
    );
    if (user) {
      alert(`Your password is: ${user.password}`);
    } else {
      alert('User not found');
    }
  }

  getCurrentUser(): Observable<IUser> {
    return this.currentUserSubject.asObservable();
  }

  getIsLoggedIn(): Observable<boolean> {
    return this.isLoggedIn.asObservable();
  }

  getUserList(): IRegister[] {
    this.getDataFromLocalStorage('signupList');
    return this.userList;
  }
}
