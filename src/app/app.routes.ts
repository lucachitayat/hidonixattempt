import { Routes } from '@angular/router';
import { LoginViewComponent } from './pages/users/login-view/login-view.component';
import { RegisterViewComponent } from './pages/users/register-view/register-view.component';
import { ExpoManagementComponent } from './pages/expo-management/expo-management.component';
import { ForgotPasswordViewComponent } from './pages/users/forgot-password-view/forgot-password-view.component';
import { guestGuard } from './guards/guest.guard';
import { authGuard } from './guards/auth.guard';
import { AdminDashboardComponent } from './pages/dashboards/admin-dashboard/admin-dashboard.component';
import { adminGuard } from './guards/admin.guard';
import { SectorsViewComponent } from './pages/sectors-view/sectors-view.component';

export const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },

  //nested routing needed for /users?
  { path: 'login', component: LoginViewComponent, canActivate: [guestGuard] },
  {
    path: 'register',
    component: RegisterViewComponent,
    canActivate: [guestGuard],
  },
  {
    path: 'forgot',
    component: ForgotPasswordViewComponent,
    canActivate: [guestGuard],
  },

  //content routing
  {
    path: 'pavilions',
    component: ExpoManagementComponent,
    canActivate: [authGuard],
  },
  {
    path: 'sectors',
    component: SectorsViewComponent,
    canActivate: [authGuard],
  },

  //admin routing
  {
    path: 'admin',
    component: AdminDashboardComponent,
    canActivate: [adminGuard],
  },
];
