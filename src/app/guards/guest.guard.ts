import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

export const guestGuard: CanActivateFn = (route, state) => {
  const authService = inject(AuthService);
  const router = inject(Router);

  authService.getIsLoggedIn().subscribe({
    next: (status) => {
      if (status) {
        router.navigate(['pavilions']);
      }
    },
  });
  return true;
};
